<?php
set_time_limit(0);
// error_reporting(0);

define('HASHTAG_NAMESPACE', '#pvp_'); /* Sửa hashtag tại đây , chữ thường không viết hoa*/
$token = 'access_token_1'; /* Thêm token của mình hoặc người khác */
$idgroup = 'id_group'; /* Nhập Id Group tại đây */

$post = json_decode(request('https://graph.facebook.com/' .$idgroup. '/feed?fields=id,message,created_time,from&limit=100&access_token=' . $token), true); /* Lấy các bài đăng gần nhất */
$timelocpost = date('Y-m-d'); /* Lấy thời gian hiện tại */
$logpost     = file_get_contents("log.txt"); /* Ghi vào log các id bài đăng đã được nhắc để tránh lặp lại */
$limitpost = 5; // Nhập số lượng giới hạn bài viết cần kiểm tra hashtag trong 1 lần cron
for ($i = 0; $i < $limitpost; $i++) {
    $idpost      = $post['data'][$i]['id'];
    $messagepost = $post['data'][$i]['message'];
    $time        = $post['data'][$i]['created_time'];
    /* Check time Post */
    if (strpos($time, $timelocpost) !== false) {
        /* Check hashtag */
        if (strpos(strtolower($messagepost), HASHTAG_NAMESPACE) === FALSE) {
            /* Check trùng */
            if (strpos($logpost, $idpost) === FALSE) {
                /* Nhắc thêm hashtag */
                $name = $post['data'][$i]['from']['name']; // Lấy tên người đăng bài
                $comment = 'Ây da quên gì thì quên chứ đừng quên hashtag chứ hả :) ' . $name . '!' . "\r\n" . "#pvp_nguyenphat";
                request('https://graph.facebook.com/' . urlencode($idpost) . '/comments?method=post&message=' . urlencode($comment) . '&access_token=' . $token);
                $luulog = fopen("log.txt", "a");
                fwrite($luulog, $idpost . "\n"); /* Ghi log */
                fclose($luulog);
            } else {
                echo 'Đã nhắc hashtag!';
            }
        }
    
    }
}
/* Hàm request url tới Facebook */
function request($url) {
    if (!filter_var($url, FILTER_VALIDATE_URL)) {
        return FALSE;
    }
    $options = array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => TRUE,
        CURLOPT_HEADER => FALSE,
        CURLOPT_FOLLOWLOCATION => TRUE,
        CURLOPT_ENCODING => '',
        CURLOPT_USERAGENT => 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.87 Safari/537.36',
        CURLOPT_AUTOREFERER => TRUE,
        CURLOPT_CONNECTTIMEOUT => 15,
        CURLOPT_TIMEOUT => 15,
        CURLOPT_MAXREDIRS => 5,
        CURLOPT_SSL_VERIFYHOST => 2,
        CURLOPT_SSL_VERIFYPEER => 0
    );
    $ch = curl_init();
    curl_setopt_array($ch, $options);
    $response  = curl_exec($ch);
    $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);
    unset($options);
    return $http_code === 200 ? $response : FALSE;
}
?>